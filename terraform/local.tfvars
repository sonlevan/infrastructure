env = "sit"

nginx_ingress_controller_enabled  = true
apisix_ingress_controller_enabled = true
apisix_dashboard_enabled          = true
gitlab_runner_enabled             = true

gitlab-docker-username = "tranhuuhoa1481986"
gitlab-docker-password = "glpat-ikxFLKTFsiQ1BtgbQKFn"
gitlab-docker-server   = "https://registry.gitlab.com"
gitlab-docker-email    = "tranhuuhoa1481986@gmail.com"

db-username       = "admin"
db-password       = "secretpassword"
db-admin-password = "secretpassword"
db-name           = "digital_banking_db"

eureka_uri              = "http://eureka-service/eureka"
postgres_datasource_uri = "postgres-postgresql"
kafka_bootstrap_servers = "debezium-cluster-kafka-bootstrap.kafka-cdc.svc.cluster.local"
jms_queue_uri           = "amq-broker-rhel8"
lra_coordinator_url     = "lra-coordinator-service"
test_service_uri        = "http://test-service/"

internal_tranfer_service_uri = "internal-tranfer-service"
limit_service_uri            = "limit-service"
mock_integration_service_uri = "mock-integration-service"

keycloak_issuer_uri = "http://keycloak/keycloak/realms/digital_realm"
keycloak_realm_cert = "http://keycloak/keycloak/realms/digital_realm/protocol/openid-connect/certs"
