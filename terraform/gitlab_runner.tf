module "gitlab_runner" {
  count = var.gitlab_runner_enabled ? 1 : 0

  depends_on = [
    module.gitlab-registry-credentials
  ]
  source = "./modules/runner"
}
