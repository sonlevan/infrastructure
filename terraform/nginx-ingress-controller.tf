resource "helm_release" "nginx-ingress-controller" {
  count      = var.nginx_ingress_controller_enabled ? 1 : 0
  name       = "nginx-ingress-controller"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"

  set {
    name  = "ingressClassResource.name"
    value = "default"
  }

  //guide for kind https://github.com/kubernetes-sigs/kind/issues/2003
  set {
    name  = "service.type"
    value = "NodePort" # need for local access
  }
  set {
    name  = "hostNetwork"
    value = "true" # need for local access
  }
  set {
    name  = "extraArgs.publish-status-address"
    value = "localhost" # need for local access
  }
}
