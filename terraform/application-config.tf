resource "kubernetes_config_map_v1" "application-configuration-configmap" {
  metadata {
    name = "application-configuration-configmap"
  }

  data = {
    KEYCLOAK_ISSUER_URI          = "${var.keycloak_issuer_uri}"
    KEYCLOAK_REALM_CERT          = "${var.keycloak_realm_cert}"
    TEST_SERVICE_URI             = "${var.test_service_uri}"
    EUREKA_URI                   = "${var.eureka_uri}"
    POSTGRES_DATASOURCE_URI      = "${var.postgres_datasource_uri}"
    KAFKA_BOOTSTRAP_SERVERS      = "${var.kafka_bootstrap_servers}"
    JMS_QUEUE_URI                = "${var.jms_queue_uri}"
    LRA_COORDINATOR_URL          = "${var.lra_coordinator_url}"
    INTERNAL_TRANFER_SERVICE_URI = "${var.internal_tranfer_service_uri}"
    LIMIT_SERVICE_URI            = "${var.limit_service_uri}"
    MOCK_INTEGRATION_SERVICE_URI = "${var.mock_integration_service_uri}"
  }
}
