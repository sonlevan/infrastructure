variable "env" {}

variable "nginx_ingress_controller_enabled" {}
variable "apisix_ingress_controller_enabled" {}
variable "apisix_dashboard_enabled" {}

variable "gitlab_runner_enabled" {}
variable "gitlab-docker-username" {}
variable "gitlab-docker-password" {}
variable "gitlab-docker-server" {}
variable "gitlab-docker-email" {}

variable "db-username" {}
variable "db-password" {}
variable "db-admin-password" {}
variable "db-name" {}

variable "keycloak_issuer_uri" {}
variable "test_service_uri" {}
variable "keycloak_realm_cert" {}
variable "jms_queue_uri" {}
variable "lra_coordinator_url" {}
variable "internal_tranfer_service_uri" {}
variable "limit_service_uri" {}
variable "mock_integration_service_uri" {}

variable "eureka_uri" {}
variable "postgres_datasource_uri" {}
variable "kafka_bootstrap_servers" {}
