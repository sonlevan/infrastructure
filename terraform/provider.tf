provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kind-mscluster"
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "kind-mscluster"
  }
}
